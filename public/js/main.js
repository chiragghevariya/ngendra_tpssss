$.ajaxSetup({
	headers:{
		'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
	}
});
$(document).ready(function(){
	$('#register-btn').click(function(e){
		e.preventDefault();

		$.ajax({
			type:"POST",
			url:"register",
			data:$('#register-form').serialize(),
			success:function (data) {
				if(data)
				{	
					$('#otp-form').prepend('<input type="hidden" name="id" value="'+data+'">');
					$('#modal-1').modal('show');
				}
			}
		});
	});
	
	    $("#myBtn").click(function(){
	        $("#myModal").modal();
	    });
	 
});
function initMap() {  
	  var lat_lng = {lat:22.258652, lng: 71.19238050000001};  
	  var directionsService = new google.maps.DirectionsService;    
	  var directionsDisplay = new google.maps.DirectionsRenderer;    
	  var map = new google.maps.Map(document.getElementById('map'), {    
	    zoom: 8,    
	    center: lat_lng    
	  });    
	  directionsDisplay.setMap(map);    
	    
	  var onChangeHandler = function() {    
	    calculateAndDisplayRoute(directionsService, directionsDisplay);    
	  };  
	 	document.getElementById('search-btn').addEventListener('click',onChangeHandler);  
	}    
	    
	function calculateAndDisplayRoute(directionsService, directionsDisplay) {    
	  directionsService.route({    
	    origin:document.getElementById('Source').value,    
	    destination: document.getElementById('Destination').value,    
	    travelMode: google.maps.TravelMode.DRIVING    
	  }, function(response, status) {    
	    if (status === google.maps.DirectionsStatus.OK) {    
	      directionsDisplay.setDirections(response);    
	    } else {    
	      window.alert('Request for getting direction is failed due to ' + status);    
	    }    
	  });    
	}    