@extends('master.master')

@section('style')
	<link rel="stylesheet" href="{{asset('public/css/sky-forms.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/self.css')}}">

@endsection

@section('content')
<div id="wrapper">  
	<header id="header">
		@include('layout.nav')
		@include('layout.header')
	</header>
	@yield('page-content')	
	   
	@include('layout.footer')
</div>
@endsection

@section('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?signed_in=true&callback=initMap&key=" async defer></script> 
	<script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/main.js')}}"></script>
@endsection
