@extends('pages.index')
<?php $title = "Contact Us"; ?>
@section('page-content')

	<!--<div class="container">
		<h1 align="center">This is page under construction </h1>
		<h2 align="center">Please try after sometimes</h2>
	</div> -->
	<div class="layout-2cols sidebar-right clearfix contact-us-page">
        <div class="content grid_8">
            <div class="wrap-contact-form">
                <div class="common-title mr-grid">Contact Form</div>

                <div class="clearfix form contact-form">
                   <form id="contact-form" class="sky-form" action="{{route('contactus.store')}}" method="post">
                       {{csrf_field()}}
                   <fieldset>
                       <div class="grid_6">
                           <label class="lbl-wrap" for="txt-name">
                               <input id="txt-name" class="txt" type="text" name="name" placeholder="Name" />
                           </label>
                       </div>
                       <div class="grid_6">
                           <label class="lbl-wrap" for="txt-email-address">
                               <input id="txt-email-address" class="txt" type="email" name="email" placeholder="Email Address"/>
                           </label>
                       </div>
                       <div class="grid_6">
                           <label class="lbl-wrap" for="txt-phone">
                               <input id="txt-phone" maxlength="10" class="txt" type="text" name="phone" placeholder="Phone"/>
                           </label>
                       </div>
                       <div class="grid_6">
                           <label class="lbl-wrap" for="txt-website">
                               <input id="txt-website" class="txt" type="text" name="website" placeholder="Website"/>
                           </label>
                       </div>
                       <div class="grid_12">
                           <label class="lbl-wrap" for="txt-message">
                               <textarea class="txt" id="txt-message" cols="30" rows="10" name="message" placeholder="Message"></textarea>
                           </label>
                       </div>
                       <div class="grid_12">
                           <p class="rs ta-r clearfix">
                               <span id="response"></span>
                               <input type="submit" id="submit-contact" class="btn-blue smaller" value="Submit"/>
                           </p>
                       </div>
                       </fieldset>
                   </form>
                </div>
            </div>
        </div>
        <div class="sidebar grid_4">
            <div class="wrap-our-information">
                <div class="our-information lh-heigher">
                    <p class="rs info-item">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis.</p>
                    <p class="rs info-item">
                        <span class="fw-b">Address:</span> 111 lorem St. 5th Floor,<br />
                        Ipsum City, MA 00001
                    </p>
                    <p class="rs info-item">
                        <span class="fw-b">Phone:</span> +1 (555) 55-55-555<br />
                        (9AM - 6PM EST)
                    </p>
                    <p class="rs info-item"><span class="fw-b">Email:</span> <a class="fc-blue" href="http://envato.megadrupal.com/cdn-cgi/l/email-protection#d9aab8b5bcaa99bbbcaaadaebcbbaab6bfadf7bab6b4"><span class="__cf_email__" data-cfemail="1764767b72645775726463607275647871633974787a">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
                </div>
            </div>
        </div>
    </div>




@include('pages.auth.login')	
@endsection