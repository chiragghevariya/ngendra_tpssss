@extends('master.master')
@extends('pages.index')
<?php $title = "Registration"; ?>

@section('page-content')
    <div class="container register-form">
        <div class="body body-s">
            <form action="#" id="register-form" method="post" class="sky-form">
            {{csrf_field()}}
                <header>Registration form</header>
                
                <fieldset>                  
                    <section>
                        <label class="input">
                            <i class="icon-append icon-user"></i>
                            <input type="text" name="name" placeholder="Username">
                            <b class="tooltip tooltip-bottom-right">Enter your fullname</b>
                        </label>
                    </section>
                        <section>
                        <label class="input">
                            <i class="icon-append icon-mobile-phone"></i>
                            <input type="text" placeholder="Mobile no" id="contact" name="mobile">
                            <b class="tooltip tooltip-bottom-right">Enter Mobile numbers</b>
                        </label>
                    </section>
                    <section>
                        <label class="input">
                            <i class="icon-append icon-envelope-alt"></i>
                            <input type="text" name="email" placeholder="Email address">
                            <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                        </label>
                    </section>
                    
                    <section>
                        <label class="input">
                            <i class="icon-append icon-lock"></i>
                            <input type="password" name="password" placeholder="Password">
                            <b class="tooltip tooltip-bottom-right">use combination of characters , numbers and expression</b>
                        </label>
                    </section>
                    <section class="select">
                        <label class="select">
                            <select name="gender">
                                <option value="0" selected disabled>Gender</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                                <option value="other">Other</option>
                            </select>
                            <i></i>
                        </label>
                    </section>
                </fieldset>
                <footer>
                    <button type="submit" data-toggle='modal' class="button" id="register-btn">SEND OTP</button>
                </footer>
            </form>
        </div>
    </div>
    <!--model-->
<div id="modal-1" class="modal fade" role='dialog'>
    <div class="modal-dialog">
    <!--model content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal'> X</button>
                   <h4 class="modal-title">OTP verification code</h4>
            </div>
            <div class="modal-body">
                <Form method='POST' id="otp-form" action="{{route('verifyotp',['traveller'])}}" class="form-horizontal">
                {{csrf_field()}}
                {{method_field('post')}}
                    <div class="form-group">
                      <label for="firstname" class="col-sm-4 control-label">OTP</label>
                      <div class="col-sm-5">
                         <input type="text" name='otpcode' class="form-control" id="firstname" placeholder="Enter your OTP here">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-offset-4 col-sm-10">
                         <button type="submit" class="btn-def">Sign in</button>
                      </div>
                   </div>
                </Form> 
            </div>
        </div>
        <!--end content-->
    </div>
 </div>
 <!--end model-->
 @include('pages.auth.login')
@endsection