
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="body body-s">
    
      <form method="post" action="{{route('auth.login')}}" class="sky-form">
      {{csrf_field()}}
      {{method_field('post')}}
        <header>Login form</header>
        
        <fieldset>          
          <section>
            <div class="row">
              <label class="label col col-4">E-mail</label>
              <div class="col col-8">
                <label class="input">
                  <i class="icon-append icon-user"></i>
                  <input type="email" name="email">
                </label>
              </div>
            </div>
          </section>
          
          <section>
            <div class="row">
              <label class="label col col-4">Password</label>
              <div class="col col-8">
                <label class="input">
                  <i class="icon-append icon-lock"></i>
                  <input type="password" name="password">
                </label>
                <div class="note"><a href="#">Forgot password?</a></div>
              </div>
            </div>
          </section>
          
          <section>
            <div class="row">
              <div class="col col-4"></div>
              <div class="col col-8">
                <label class="checkbox"><input type="checkbox" name="checkbox-inline" checked><i></i>Keep me logged in</label>
              </div>
            </div>
          </section>
        </fieldset>
        <footer>
          <button type="submit" class="button">Log in</button>
          <a href="#" class="button button-secondary">Register</a>
        </footer>
      </form>
      </div>
    </div>
  </div> 
</div>
