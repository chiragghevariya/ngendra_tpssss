    <span class="top-line"></span>
        <div class="container_12">
            <div class="top-login-reg">
            @if(!Auth::user())
                <a id="myBtn" href="#">Login</a>
                <a href="{{URL::asset('traveller/register')}}">Register</a>
            @else
                <a href="{{URL::asset('logout')}}">Logout</a>
            @endif
            </div>
            <div class="clear"></div>
            <div class="head-menu clearfix">
                <h1 id="logo">
                    <a href="{{URL::asset('')}}">
                        <img src="public/images/Latest Logo1.png">
                    </a>
                </h1>
                <nav class="main-nav">
                    <ul id="main-menu" class="nav nav-horizontal">
                        <li class="active">
                            <a href="{{URL::asset('')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{URL::asset('/toll-plazas-on-map')}}">Toll Plazas On Map</a>
                        </li>
                        <li>
                            <a href="{{URL::asset('/toll-plazas-at-glance')}}">Toll Plazas At Glance</a>
                        </li>
                        <li class="has-sub">
                            <a href="">Trip</a>
                            <ul class="sub-menu">
                            <li><a href="{{URL::asset('/New-Trip')}}">New Trip</a></li>
                            <li><a href="{{URL::asset('/Vehicle-details')}}">Vehicle Details</a></li>
                                <li><a href="{{URL::asset('/Previous-trip')}}">Previous Trip</a></li>
                            </ul>
                        </li>
                          <li class="has-sub">
                            <a href="">User Help</a>
                            <ul class="sub-menu">
                                <li><a href="#">User Manual</a></li>
                                <li><a href="{{URL::asset('/feedback')}}">Feedback</a></li>
                                <li><a href="{{URL::asset('/complain')}}">Complain</a></li>
                            </ul>
                        </li>
                        <li><a href="{{route('contactus.index')}}">Contact Us</a>
                        </li>
                      
                    </ul>
                    <a id="btn-toogle-menu" class="btn-toogle-menu" href="#alternate-menu">
                        <span class="line-bar"></span>
                        <span class="line-bar"></span>
                        <span class="line-bar"></span>
                    </a>
                    <div id="right-menu">
                        <ul class="alternate-menu">
                            <li><a href="">Home</a></li>

                            <li><a href="{{URL::asset('/toll-plazas-on-map')}}">Toll Plazas On Map</a></li>

                            <li><a href="{{URL::asset('/toll-plazas-at-glance')}}">Toll Plazas At Glance</a></li>

                            <li class="has-sub"><a href="">Trip</a>
                            <ul class="sub-menu">
                            <li><a href="{{URL::asset('/New-Trip')}}">New Trip</a></li>
                            <li><a href="{{URL::asset('/Vehicle-details')}}">Vehicle Details</a></li>
                                <li><a href="{{URL::asset('/Previous-trip')}}">Previous Trip</a></li>
                            </ul>
                            </li>

                            <li class="has-sub">
                            <a href="">User Help</a>
                            <ul class="sub-menu">
                                <li><a href="#">User Manual</a></li>
                                <li><a href="{{URL::asset('/feedback')}}">Feedback</a></li>
                                <li><a href="{{URL::asset('/complain')}}">Complain</a></li>
                            </ul>
                        </li>
                            <li><a href="{{URL::asset('/contact-us')}}">Contact Us</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        
        <div class="contentborder">
           <img id="contentborder">
        </div>
     
         <!--   <div class="sequence-theme">
                <div id="sequence">
                    <div class="wrap-pagination">          
                    </div>
                </div>
            </div>-->
          