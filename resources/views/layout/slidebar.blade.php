
<div class="head-about-us container_12 clearfix ">
    <div class="grid_12 content-about clearfix">
        <div id="jssor_1" class="slide-main">
        <!-- Loading Screen -->
            <div data-u="slides" class="slide-img">
        
                <div data-p="225.00">
                    <img data-u="image" src="public/images/t1.jpg" />
                 </div> 
                <div data-p="225.00">
                    <img data-u="image" src="public/images/t2.jpg" />
                </div>
                <div data-p="225.00" data-po="80% 55%" style="display:none;">
                    <img data-u="image" src="public/images/t3.jpg" />
                </div>
                <div data-p="225.00">
                    <img data-u="image" src="public/images/t4.jpg" />
                </div> 
                <div data-p="225.00" style="display:none;">
                    <img data-u="image" src="public/images/t5.jpg" />
                </div>
                <div data-p="225.00" data-po="80% 55%" style="display:none;">
                    <img data-u="image" src="public/images/t6.jpg" />
                </div>
                <div data-p="225.00" data-po="80% 55%" style="display:none;">
                    <img data-u="image" src="public/images/t7.jpg" />
                </div>
            </div>
        <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05 slide-bullet"  data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>
    </div>
</div>       
<!--<div class="top-slide">
    <div class="sequence-theme">
        <div id="sequence">
            <div class="wrap-pagination">          
            </div>
        </div>
    </div>
</div>  -->