<footer id="footer">

    <div class="info-line">
            <div class="container_12">
            <div class="clear"></div>
            </div>
    </div>
        <div class="contentborder">
           <img id="contentborder">
         </div>

    <div class="container_12 main-footer">

            <div>
                <div class="inner" style="text-align: center;">
                    <h3 class="rs common-title">Menu</h3>

                    <ul class="rs">
                        <a href="">Home</a> |
                        <a href="{{URL::asset('/toll-plazas-on-map')}}"">View Toll Plazas On Map</a> |
                        <a href="{{URL::asset('/toll-plazas-at-glance')}}">List of Toll Plazas</a> |
                        <a href="#">User Guildeline</a> |
                        <a href="{{URL::asset('/feedback')}}">Feedback</a> |
                        <a href="{{URL::asset('/contact-us')}}">Contact us</a>
            
                    </ul>
                </div>
            </div><!--end: .foot-menu -->
            <div class="clear"></div>
            <div class="lst-btn-social clearfix">
            </div>
            <p class="rs copyright"><span class="lbl">Developed by </span> <a href="" class="val" title="Toll Project company" target="_blank">5F Group.</a></p>
        </div>
</footer><!--end: #footer -->
