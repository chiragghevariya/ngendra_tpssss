@extends('master.master')
<?php $title = "Online Toll Payment system"; ?>
@section('style')
	<link rel="stylesheet" href="{{asset('public/css/sky-forms.css')}}">
	
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/slider.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/slide-ele.css')}}">
	<link rel="stylesheet" href="{{asset('public/css/self.css')}}">
	<script type="text/javascript" src="{{asset('public/js/slider.js')}}"></script>	
@endsection

@section('content')
<div id="wrapper">  
	<header id="header">
		@include('layout.nav')
		@include('layout.slidebar')
	</header>
	@yield('page-content')	

<div class="container">
	<div class="row">
		<div class="col-sm-3">
  			<div class="body body-s src-dest">
    			<form class="sky-form">
      				<fieldset>
        				<section>
          					<div class="row">
          						<!--ContentHeader-->

                      <div class="contentheader">

                      <h3 align="center">Map And Toll Plaza</h3>
                          <img id="contentheader">
                      </div><br>
          						<h1 id="logo">
                   				 <a href="{{URL::asset('toll-plazas-on-map')}}">
                       			 <img src="public/images/gujarat map.jpg">
                    			</a>
               					 </h1>
          					</div> 
        				</section>
       				</fieldset>
    			</form>
  			</div>
		</div>
		<div class="col-sm-6">
  			<div class="body body-s src-dest">
    			<form class="sky-form" method="post">
      				<fieldset>
        				<section>
          					<div class="row">
          
                        <div class="contentheader">
                        <h3 align="center">Search Route</h3>
                          <img id="contentheader">
                      </div><br>
                        <div class="clearfix form contact-form">
                          <div class="grid_4">
                           <label class="lbl-wrap" for="txtsource">
                               <input id="txtsource" class="txt" type="text" name="name" placeholder="Source" />
                           </label>
                          </div>
                          <div class="grid_1">
                           <label class="lbl-wrap" for="To"><h3 align="center">To</h3>
                           </label>
                          </div>

                       <div class="grid_4">
                           <label class="lbl-wrap" for="txtdestination">
                               <input id="txtdestination" class="txt" type="text" name="destination" placeholder="destination"/>
                           </label>
                       </div>
                       <div class="grid_2">
                           <p class="rs ta-r clearfix">
                               <span id="response"></span>
                               <input type="submit" id="submit-contact" class="btn-blue smaller" value="Submit"/>
                           </p>
                       </div>
                       <div class="grid_4">
                           <label class="lbl-wrap" for="txtdestination">Source  
                           </label>
                           <label class="lbl-wrap" for="txtdestination">Destination  
                           </label>
                           <label class="lbl-wrap" for="txtkhh">Km/h  
                           </label>
                           <label class="lbl-wrap" for="txttollplaza">Toll Plazas 
                           </label>
                       </div>
                       <div class="grid_4">
                           <label class="lbl-wrap" for="txtdestination">Bhavnagar
                           </label>
                           <label class="lbl-wrap" for="txtdestination">Ahemdabad  
                           </label>
                           <label class="lbl-wrap" for="txtkhh">205 
                           </label>
                           <label class="lbl-wrap" for="txttollplaza">1 
                           </label>
                       </div>
          					   </div> 
        				</section>
       				</fieldset>
    			</form>
  			</div>
		</div>
		<div class="col-sm-3">
  			<div class="body body-s src-dest">
    			<form class="sky-form">
      				<fieldset>
        				<section>
          					<div class="" lass="row">
                				
                        <div class="contentheader">
                        <h3 align="center">Latest News</h3>
                          <img id="contentheader">
                      </div><br>
                				<marquee direction="up" scrollamount="3" onmouseover="stop();"  onmouseout="start();">
                					<h4 class="h4">Hello come from the latest news bar manu.</h4><br>
                					<h4 class="h4">Recurtiment of toll plazas employee details.</h4><br>
                					<h4 class="h4">Toll payment mode available mode 1.Netbanking, 2.creidt card.</h4><br>
                          <h4 class="h4">Online toll payment system user consider to you allocate route.</h4><br>
                          <h4 class="h4">Drive carefully of your vehicle.</h4><br>
                				</marquee>
                        
          					</div> 
        				</section>
       				</fieldset>
    			</form>
  			</div>
		</div>
	</div> 
	
</div>

	@include('pages.auth.login')
	@include('layout.footer')
</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{asset('public/js/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/main.js')}}"></script>
	
@endsection
