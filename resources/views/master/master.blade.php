<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
    <link rel="icon" type="image/x-icon" href="public/images/Otps icon.ico" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{csrf_token()}}">

	<link href='http://fonts.googleapis.com/css?family=Fjalla+One|Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="{{asset('public/css/font.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/normalize.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/reset.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/style.css')}}"/>
	
	<link rel="stylesheet" href="{{asset('public/css/sequencejs-theme.modern-slide-in.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/jquery.sidr.dark.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/responsive.css')}}"/>
	<link rel="stylesheet" href="{{asset('public/css/bootstrap.min.css')}}">
    @yield('style')

</head>
<body>

	@yield('content')

	<script type="text/javascript" src="{{asset('public/js/jquery-1.9.1.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/jquery.tweet.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/jquery.sequence-min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/jquery.sidr.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('public/js/script.js')}}"></script>
	@yield('scripts')
</body>
</html>