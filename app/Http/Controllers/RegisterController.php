<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\User;

class RegisterController extends Controller
{
	public function getRegister()
	{
		return view('pages.Auth.register');
	}
    public function postRegister(Request $request,$role)
    {
        $user = new User;

        $user->name =$request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->password = bcrypt($request->password);
        $user->gender = $request->gender;
        $user->role = $role;
        $user->save();
        
        return redirect($role.'/sendotp/register')->withInput();
    }
}
