<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function postLogin(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return Redirect::to('traveller/toll-plazas-on-map');
        }
        else{
            return "false";
        }
    }

    public function logout()
    {
        Auth::logout();
        return back();
    }
}
