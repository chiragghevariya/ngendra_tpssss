<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Ticket;
class TicketController extends Controller
{
	public function index()
	{
 			 $Tickets = Ticket::where('user_id',\Auth::user()->id)->get();
 			
 			 return view('Traveller.index')->with('tickets',$Tickets);	
	}
    public function create()
    {
    	return view('Traveller.create');
    }

    public function store(Request $req)
    {
    	 	$ticket = new Ticket();
    	 	$ticket->user_id = \Auth::user()->id;
    	 	$ticket->checkpoints = $req->checkpoints;
    	 	$ticket->save();
    	 	return "success";
    }
}
