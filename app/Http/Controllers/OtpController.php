<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Otp;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OtpController extends Controller
{
    	public function sendMessage(Request $request,$role)
	{
     	$otp_code = strtoupper(bin2hex(openssl_random_pseudo_bytes(3)));
     	$mobile = $request->old('mobile');
      
      $otpObject = new Otp;

      $user = User::where('mobile',$mobile)->first();

      $message = $otpObject->sendSMS($mobile, $otp_code);

      	if($message == 'success')
      	{	$otpObject->user_id = $user->id;
      		$otpObject->otp_value = $otp_code;
      	  $otpObject->save();
         	return $user->id;
        }
    }
    public function verifyOtp(Request $request,$role)
    {
      $id=$request->id;
      $otp=$request->otpcode;

      $otp_record = Otp::where('user_id',$id)->first();

      if($otp_record->otp_value==$otp)
      {
          $otp_id=$otp_record->id;
          Otp::where('id',$otp_id)->update(['verified'=>true]);
          $user = User::find($id);
          if($role=='traveller')
          {
            Auth::login($user);
            return Redirect::to('traveller/toll-plazas-on-map');
          }
          else{
            Auth::login($user);
            return Redirect::to('tollemploye');
          }
      }
      else{
          return back()->with('wrongotp','wrong')
                             ->with('user_id',$id);        
          } 	 
    }
}
