<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('', function () {
    return view('pages.first.create');
});


Route::group(['middleware'=>['guest']],function(){
	
	Route::post('/login',[
		'as'=>'auth.login',
		'uses'=>'LoginController@postLogin']);
	
	Route::get('traveller/register','RegisterController@getRegister');

	Route::post('{role}/register',[
		'as'=>'auth.register',
		'uses'=>'RegisterController@postRegister'
		]);

	Route::get('{role}/sendotp/register','OtpController@sendMessage');

	Route::post('{role}/verifyOtp/register',[
		'as'=>'verifyotp',
		'uses'=>'OtpController@verifyOtp'
		]);
});

Route::get('logout','LoginController@logout');

Route::group(['middleware'=>'auth'],function() 
{
	Route::resource('tickets','TicketController');
});
Route::get('/toll-plazas-on-map',function(){
	return view('pages.toll-plazas-on-map');
});

Route::get('/toll-plazas-at-glance',function(){
	return view('pages.toll-plazas-at-glance');
});
Route::get('/trip',function(){
	return view('pages.trip');
});
Route::get('/feedback',function(){
	return view('pages.feedback');
});
Route::get('/complain',function(){
	return view('pages.complain');
});
//Route::get('/contact-us',function(){
//	return view('pages.contact-us');
//});


Route::get('/Vehicle-details',function(){
	return view('pages.Vehicle-details');
});
Route::get('/Previous-trip',function(){
	return view('pages.Previous-trip');
});
Route::get('/New-Trip',function(){
	return view('pages.New-Trip');
});

Route::resource('contactus','ContactController');


